package org.xtext.example.pessoa

class Hasher {
	
	static val int HASH_TABLE_SIZE = 1000000;
	
	def static int hash(String senha){
		
		var mul = 0 as int
		var sum = 0 as int
		
		for (i : 0 ..< senha.length) {
			mul += (i % 4 == 0) ? 1 : mul * 256;
			sum += senha.charAt(i) * mul;
		}
		
		if(sum < 0){
			sum *= -1;
		}
	  
	  	return (sum % HASH_TABLE_SIZE);
	}
	
}