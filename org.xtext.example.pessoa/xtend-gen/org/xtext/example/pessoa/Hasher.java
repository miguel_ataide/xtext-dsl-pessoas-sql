package org.xtext.example.pessoa;

import org.eclipse.xtext.xbase.lib.ExclusiveRange;

@SuppressWarnings("all")
public class Hasher {
  private static final int HASH_TABLE_SIZE = 1000000;
  
  public static int hash(final String senha) {
    int mul = ((int) 0);
    int sum = ((int) 0);
    int _length = senha.length();
    ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _length, true);
    for (final Integer i : _doubleDotLessThan) {
      {
        int _mul = mul;
        int _xifexpression = (int) 0;
        if ((((i).intValue() % 4) == 0)) {
          _xifexpression = 1;
        } else {
          _xifexpression = (mul * 256);
        }
        mul = (_mul + _xifexpression);
        int _sum = sum;
        char _charAt = senha.charAt((i).intValue());
        int _multiply = (_charAt * mul);
        sum = (_sum + _multiply);
      }
    }
    if ((sum < 0)) {
      int _sum = sum;
      sum = (_sum * (-1));
    }
    return (sum % Hasher.HASH_TABLE_SIZE);
  }
}
