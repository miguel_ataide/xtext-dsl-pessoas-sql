package org.xtext.example.pessoa.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.pessoa.services.PessoaGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPessoaParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Pessoa'", "';'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalPessoaParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPessoaParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPessoaParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPessoa.g"; }


    	private PessoaGrammarAccess grammarAccess;

    	public void setGrammarAccess(PessoaGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRulePessoas"
    // InternalPessoa.g:53:1: entryRulePessoas : rulePessoas EOF ;
    public final void entryRulePessoas() throws RecognitionException {
        try {
            // InternalPessoa.g:54:1: ( rulePessoas EOF )
            // InternalPessoa.g:55:1: rulePessoas EOF
            {
             before(grammarAccess.getPessoasRule()); 
            pushFollow(FOLLOW_1);
            rulePessoas();

            state._fsp--;

             after(grammarAccess.getPessoasRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePessoas"


    // $ANTLR start "rulePessoas"
    // InternalPessoa.g:62:1: rulePessoas : ( ( rule__Pessoas__BaseDadosAssignment )* ) ;
    public final void rulePessoas() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:66:2: ( ( ( rule__Pessoas__BaseDadosAssignment )* ) )
            // InternalPessoa.g:67:2: ( ( rule__Pessoas__BaseDadosAssignment )* )
            {
            // InternalPessoa.g:67:2: ( ( rule__Pessoas__BaseDadosAssignment )* )
            // InternalPessoa.g:68:3: ( rule__Pessoas__BaseDadosAssignment )*
            {
             before(grammarAccess.getPessoasAccess().getBaseDadosAssignment()); 
            // InternalPessoa.g:69:3: ( rule__Pessoas__BaseDadosAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPessoa.g:69:4: rule__Pessoas__BaseDadosAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Pessoas__BaseDadosAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getPessoasAccess().getBaseDadosAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePessoas"


    // $ANTLR start "entryRulePessoa"
    // InternalPessoa.g:78:1: entryRulePessoa : rulePessoa EOF ;
    public final void entryRulePessoa() throws RecognitionException {
        try {
            // InternalPessoa.g:79:1: ( rulePessoa EOF )
            // InternalPessoa.g:80:1: rulePessoa EOF
            {
             before(grammarAccess.getPessoaRule()); 
            pushFollow(FOLLOW_1);
            rulePessoa();

            state._fsp--;

             after(grammarAccess.getPessoaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePessoa"


    // $ANTLR start "rulePessoa"
    // InternalPessoa.g:87:1: rulePessoa : ( ( rule__Pessoa__Group__0 ) ) ;
    public final void rulePessoa() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:91:2: ( ( ( rule__Pessoa__Group__0 ) ) )
            // InternalPessoa.g:92:2: ( ( rule__Pessoa__Group__0 ) )
            {
            // InternalPessoa.g:92:2: ( ( rule__Pessoa__Group__0 ) )
            // InternalPessoa.g:93:3: ( rule__Pessoa__Group__0 )
            {
             before(grammarAccess.getPessoaAccess().getGroup()); 
            // InternalPessoa.g:94:3: ( rule__Pessoa__Group__0 )
            // InternalPessoa.g:94:4: rule__Pessoa__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Pessoa__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPessoaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePessoa"


    // $ANTLR start "rule__Pessoa__Group__0"
    // InternalPessoa.g:102:1: rule__Pessoa__Group__0 : rule__Pessoa__Group__0__Impl rule__Pessoa__Group__1 ;
    public final void rule__Pessoa__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:106:1: ( rule__Pessoa__Group__0__Impl rule__Pessoa__Group__1 )
            // InternalPessoa.g:107:2: rule__Pessoa__Group__0__Impl rule__Pessoa__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Pessoa__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Pessoa__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__0"


    // $ANTLR start "rule__Pessoa__Group__0__Impl"
    // InternalPessoa.g:114:1: rule__Pessoa__Group__0__Impl : ( 'Pessoa' ) ;
    public final void rule__Pessoa__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:118:1: ( ( 'Pessoa' ) )
            // InternalPessoa.g:119:1: ( 'Pessoa' )
            {
            // InternalPessoa.g:119:1: ( 'Pessoa' )
            // InternalPessoa.g:120:2: 'Pessoa'
            {
             before(grammarAccess.getPessoaAccess().getPessoaKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getPessoaAccess().getPessoaKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__0__Impl"


    // $ANTLR start "rule__Pessoa__Group__1"
    // InternalPessoa.g:129:1: rule__Pessoa__Group__1 : rule__Pessoa__Group__1__Impl rule__Pessoa__Group__2 ;
    public final void rule__Pessoa__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:133:1: ( rule__Pessoa__Group__1__Impl rule__Pessoa__Group__2 )
            // InternalPessoa.g:134:2: rule__Pessoa__Group__1__Impl rule__Pessoa__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Pessoa__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Pessoa__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__1"


    // $ANTLR start "rule__Pessoa__Group__1__Impl"
    // InternalPessoa.g:141:1: rule__Pessoa__Group__1__Impl : ( ( rule__Pessoa__NomeAssignment_1 ) ) ;
    public final void rule__Pessoa__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:145:1: ( ( ( rule__Pessoa__NomeAssignment_1 ) ) )
            // InternalPessoa.g:146:1: ( ( rule__Pessoa__NomeAssignment_1 ) )
            {
            // InternalPessoa.g:146:1: ( ( rule__Pessoa__NomeAssignment_1 ) )
            // InternalPessoa.g:147:2: ( rule__Pessoa__NomeAssignment_1 )
            {
             before(grammarAccess.getPessoaAccess().getNomeAssignment_1()); 
            // InternalPessoa.g:148:2: ( rule__Pessoa__NomeAssignment_1 )
            // InternalPessoa.g:148:3: rule__Pessoa__NomeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Pessoa__NomeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPessoaAccess().getNomeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__1__Impl"


    // $ANTLR start "rule__Pessoa__Group__2"
    // InternalPessoa.g:156:1: rule__Pessoa__Group__2 : rule__Pessoa__Group__2__Impl rule__Pessoa__Group__3 ;
    public final void rule__Pessoa__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:160:1: ( rule__Pessoa__Group__2__Impl rule__Pessoa__Group__3 )
            // InternalPessoa.g:161:2: rule__Pessoa__Group__2__Impl rule__Pessoa__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Pessoa__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Pessoa__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__2"


    // $ANTLR start "rule__Pessoa__Group__2__Impl"
    // InternalPessoa.g:168:1: rule__Pessoa__Group__2__Impl : ( ( rule__Pessoa__SobrenomeAssignment_2 ) ) ;
    public final void rule__Pessoa__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:172:1: ( ( ( rule__Pessoa__SobrenomeAssignment_2 ) ) )
            // InternalPessoa.g:173:1: ( ( rule__Pessoa__SobrenomeAssignment_2 ) )
            {
            // InternalPessoa.g:173:1: ( ( rule__Pessoa__SobrenomeAssignment_2 ) )
            // InternalPessoa.g:174:2: ( rule__Pessoa__SobrenomeAssignment_2 )
            {
             before(grammarAccess.getPessoaAccess().getSobrenomeAssignment_2()); 
            // InternalPessoa.g:175:2: ( rule__Pessoa__SobrenomeAssignment_2 )
            // InternalPessoa.g:175:3: rule__Pessoa__SobrenomeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Pessoa__SobrenomeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getPessoaAccess().getSobrenomeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__2__Impl"


    // $ANTLR start "rule__Pessoa__Group__3"
    // InternalPessoa.g:183:1: rule__Pessoa__Group__3 : rule__Pessoa__Group__3__Impl rule__Pessoa__Group__4 ;
    public final void rule__Pessoa__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:187:1: ( rule__Pessoa__Group__3__Impl rule__Pessoa__Group__4 )
            // InternalPessoa.g:188:2: rule__Pessoa__Group__3__Impl rule__Pessoa__Group__4
            {
            pushFollow(FOLLOW_4);
            rule__Pessoa__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Pessoa__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__3"


    // $ANTLR start "rule__Pessoa__Group__3__Impl"
    // InternalPessoa.g:195:1: rule__Pessoa__Group__3__Impl : ( ( rule__Pessoa__CpfAssignment_3 ) ) ;
    public final void rule__Pessoa__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:199:1: ( ( ( rule__Pessoa__CpfAssignment_3 ) ) )
            // InternalPessoa.g:200:1: ( ( rule__Pessoa__CpfAssignment_3 ) )
            {
            // InternalPessoa.g:200:1: ( ( rule__Pessoa__CpfAssignment_3 ) )
            // InternalPessoa.g:201:2: ( rule__Pessoa__CpfAssignment_3 )
            {
             before(grammarAccess.getPessoaAccess().getCpfAssignment_3()); 
            // InternalPessoa.g:202:2: ( rule__Pessoa__CpfAssignment_3 )
            // InternalPessoa.g:202:3: rule__Pessoa__CpfAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Pessoa__CpfAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getPessoaAccess().getCpfAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__3__Impl"


    // $ANTLR start "rule__Pessoa__Group__4"
    // InternalPessoa.g:210:1: rule__Pessoa__Group__4 : rule__Pessoa__Group__4__Impl rule__Pessoa__Group__5 ;
    public final void rule__Pessoa__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:214:1: ( rule__Pessoa__Group__4__Impl rule__Pessoa__Group__5 )
            // InternalPessoa.g:215:2: rule__Pessoa__Group__4__Impl rule__Pessoa__Group__5
            {
            pushFollow(FOLLOW_5);
            rule__Pessoa__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Pessoa__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__4"


    // $ANTLR start "rule__Pessoa__Group__4__Impl"
    // InternalPessoa.g:222:1: rule__Pessoa__Group__4__Impl : ( ( rule__Pessoa__LoginAssignment_4 ) ) ;
    public final void rule__Pessoa__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:226:1: ( ( ( rule__Pessoa__LoginAssignment_4 ) ) )
            // InternalPessoa.g:227:1: ( ( rule__Pessoa__LoginAssignment_4 ) )
            {
            // InternalPessoa.g:227:1: ( ( rule__Pessoa__LoginAssignment_4 ) )
            // InternalPessoa.g:228:2: ( rule__Pessoa__LoginAssignment_4 )
            {
             before(grammarAccess.getPessoaAccess().getLoginAssignment_4()); 
            // InternalPessoa.g:229:2: ( rule__Pessoa__LoginAssignment_4 )
            // InternalPessoa.g:229:3: rule__Pessoa__LoginAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Pessoa__LoginAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getPessoaAccess().getLoginAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__4__Impl"


    // $ANTLR start "rule__Pessoa__Group__5"
    // InternalPessoa.g:237:1: rule__Pessoa__Group__5 : rule__Pessoa__Group__5__Impl rule__Pessoa__Group__6 ;
    public final void rule__Pessoa__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:241:1: ( rule__Pessoa__Group__5__Impl rule__Pessoa__Group__6 )
            // InternalPessoa.g:242:2: rule__Pessoa__Group__5__Impl rule__Pessoa__Group__6
            {
            pushFollow(FOLLOW_6);
            rule__Pessoa__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Pessoa__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__5"


    // $ANTLR start "rule__Pessoa__Group__5__Impl"
    // InternalPessoa.g:249:1: rule__Pessoa__Group__5__Impl : ( ( rule__Pessoa__SenhaAssignment_5 ) ) ;
    public final void rule__Pessoa__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:253:1: ( ( ( rule__Pessoa__SenhaAssignment_5 ) ) )
            // InternalPessoa.g:254:1: ( ( rule__Pessoa__SenhaAssignment_5 ) )
            {
            // InternalPessoa.g:254:1: ( ( rule__Pessoa__SenhaAssignment_5 ) )
            // InternalPessoa.g:255:2: ( rule__Pessoa__SenhaAssignment_5 )
            {
             before(grammarAccess.getPessoaAccess().getSenhaAssignment_5()); 
            // InternalPessoa.g:256:2: ( rule__Pessoa__SenhaAssignment_5 )
            // InternalPessoa.g:256:3: rule__Pessoa__SenhaAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Pessoa__SenhaAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getPessoaAccess().getSenhaAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__5__Impl"


    // $ANTLR start "rule__Pessoa__Group__6"
    // InternalPessoa.g:264:1: rule__Pessoa__Group__6 : rule__Pessoa__Group__6__Impl ;
    public final void rule__Pessoa__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:268:1: ( rule__Pessoa__Group__6__Impl )
            // InternalPessoa.g:269:2: rule__Pessoa__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Pessoa__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__6"


    // $ANTLR start "rule__Pessoa__Group__6__Impl"
    // InternalPessoa.g:275:1: rule__Pessoa__Group__6__Impl : ( ';' ) ;
    public final void rule__Pessoa__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:279:1: ( ( ';' ) )
            // InternalPessoa.g:280:1: ( ';' )
            {
            // InternalPessoa.g:280:1: ( ';' )
            // InternalPessoa.g:281:2: ';'
            {
             before(grammarAccess.getPessoaAccess().getSemicolonKeyword_6()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getPessoaAccess().getSemicolonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__Group__6__Impl"


    // $ANTLR start "rule__Pessoas__BaseDadosAssignment"
    // InternalPessoa.g:291:1: rule__Pessoas__BaseDadosAssignment : ( rulePessoa ) ;
    public final void rule__Pessoas__BaseDadosAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:295:1: ( ( rulePessoa ) )
            // InternalPessoa.g:296:2: ( rulePessoa )
            {
            // InternalPessoa.g:296:2: ( rulePessoa )
            // InternalPessoa.g:297:3: rulePessoa
            {
             before(grammarAccess.getPessoasAccess().getBaseDadosPessoaParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            rulePessoa();

            state._fsp--;

             after(grammarAccess.getPessoasAccess().getBaseDadosPessoaParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoas__BaseDadosAssignment"


    // $ANTLR start "rule__Pessoa__NomeAssignment_1"
    // InternalPessoa.g:306:1: rule__Pessoa__NomeAssignment_1 : ( RULE_ID ) ;
    public final void rule__Pessoa__NomeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:310:1: ( ( RULE_ID ) )
            // InternalPessoa.g:311:2: ( RULE_ID )
            {
            // InternalPessoa.g:311:2: ( RULE_ID )
            // InternalPessoa.g:312:3: RULE_ID
            {
             before(grammarAccess.getPessoaAccess().getNomeIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPessoaAccess().getNomeIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__NomeAssignment_1"


    // $ANTLR start "rule__Pessoa__SobrenomeAssignment_2"
    // InternalPessoa.g:321:1: rule__Pessoa__SobrenomeAssignment_2 : ( RULE_ID ) ;
    public final void rule__Pessoa__SobrenomeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:325:1: ( ( RULE_ID ) )
            // InternalPessoa.g:326:2: ( RULE_ID )
            {
            // InternalPessoa.g:326:2: ( RULE_ID )
            // InternalPessoa.g:327:3: RULE_ID
            {
             before(grammarAccess.getPessoaAccess().getSobrenomeIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPessoaAccess().getSobrenomeIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__SobrenomeAssignment_2"


    // $ANTLR start "rule__Pessoa__CpfAssignment_3"
    // InternalPessoa.g:336:1: rule__Pessoa__CpfAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Pessoa__CpfAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:340:1: ( ( RULE_STRING ) )
            // InternalPessoa.g:341:2: ( RULE_STRING )
            {
            // InternalPessoa.g:341:2: ( RULE_STRING )
            // InternalPessoa.g:342:3: RULE_STRING
            {
             before(grammarAccess.getPessoaAccess().getCpfSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getPessoaAccess().getCpfSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__CpfAssignment_3"


    // $ANTLR start "rule__Pessoa__LoginAssignment_4"
    // InternalPessoa.g:351:1: rule__Pessoa__LoginAssignment_4 : ( RULE_ID ) ;
    public final void rule__Pessoa__LoginAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:355:1: ( ( RULE_ID ) )
            // InternalPessoa.g:356:2: ( RULE_ID )
            {
            // InternalPessoa.g:356:2: ( RULE_ID )
            // InternalPessoa.g:357:3: RULE_ID
            {
             before(grammarAccess.getPessoaAccess().getLoginIDTerminalRuleCall_4_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPessoaAccess().getLoginIDTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__LoginAssignment_4"


    // $ANTLR start "rule__Pessoa__SenhaAssignment_5"
    // InternalPessoa.g:366:1: rule__Pessoa__SenhaAssignment_5 : ( RULE_STRING ) ;
    public final void rule__Pessoa__SenhaAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPessoa.g:370:1: ( ( RULE_STRING ) )
            // InternalPessoa.g:371:2: ( RULE_STRING )
            {
            // InternalPessoa.g:371:2: ( RULE_STRING )
            // InternalPessoa.g:372:3: RULE_STRING
            {
             before(grammarAccess.getPessoaAccess().getSenhaSTRINGTerminalRuleCall_5_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getPessoaAccess().getSenhaSTRINGTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Pessoa__SenhaAssignment_5"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001000L});

}